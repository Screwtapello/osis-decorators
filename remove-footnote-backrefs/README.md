remove-footnote-backrefs.py
===========================

Synopsis
========

    remove-footnote-backrefs.py INPUT OUTPUT
    remove-footnote-backrefs.py --help

Description
===========

This tool removes backreferences
from a footnote to the verse it came from.

In a print Bible,
footnotes and the text they refer to
can be separated widely on the page.
As a result,
it's helpful to have a pointer from each footnote
back to the verse it came from.

In a digital Bible,
the Bible reading software tracks where you are in the text,
so if you read a footnote
you can close the pop-up window
or hit the "Back" button
or some other action that takes you right back to where you were.
Having a pointer from the footnote back to the verse
is redundant
and can be confusing if the software opens the "cross-reference"
in a yet another window.
So, it makes sense to remove back-references in footnotes,
if any exist,
from digital Bibles.

Specifically,
this tool looks for a `<reference type="annotateRef">` element
inside a `<note>` element,
such as the `<reference>` element in the following example:

```xml
<verse osisID="Gen.1.3" n="3">
    And God said, "Let there be light,"
    <note>
        <reference type="annotateRef">1:3</reference>
        Cited in 2 Corinthians 4:6
    </note>
    and there was light.
</verse>
```

The above example would have the `<reference>` element removed,
like this:

```xml
<verse osisID="Gen.1.3" n="3">
    And God said, "Let there be light,"
    <note>
        Cited in 2 Corinthians 4:6
    </note>
    and there was light.
</verse>
```

Options
=======

INPUT
-----

The path to an OSIS XML file
to be scanned for backreferences in footnotes.

If set to the special name `-`,
the XML data will be read from stdin.

OUTPUT
------

The path will be created with the modified version of the text.
This file must not already exist.

If set to the special name `-`,
the XML data will be written to stdout.

If you really need to write the output to a file that already exists,
you can delete it beforehand,
or use `-` as the output file name
and pipe standard output into the file:

    remove-footnote-backrefs.py input.xml - > file-to-be-clobbered.xml

-h, --help
----------

If this flag is present on the command line,
the tool will print a brief summary of how to use itself,
then exit.

Configuration
=============

This tool does has no configuration options.

Hacking
=======

If you wish to modify the script,
you can run the built-in test suite like this:

    python3 -m unittest ./remove-footnote-backrefs.py
