linkify-references.py
=====================

Synopsis
========

    linkify-references.py [-v|--verbose] INPUT CONFIG OUTPUT
    linkify-references.py --help

Description
===========

This tool searches an OSIS XML file
for human-readable Bible references
(like "John 3:16")
and adds the OSIS markup required to make them machine-readable too.
It will leave existing references alone,
and try to correct broken reference markup
by either guessing what it should link to
(preserving the reference type, if any),
or by removing it and leaving the content as regular text.

It supports extensive customisation of Bible book names
and reference syntax,
so it can be adapted to detect references in languages other than English
(see "Configuration" below for details).

Options
=======

INPUT
-----

The path to an OSIS XML file
to be scanned for references.

If set to the special name `-`,
the XML data will be read from stdin.

CONFIG
------

The path to a configuration file
describing how to recognise and interpret references.
See "Configuration" below for details on the format of this file,
or look at [an example file](./example-config.ini).

You will probably need to customise this configuration
for each OSIS document you process,
so consider keeping the configuration beside
the other source files for your project
rather than trying to use one config file everywhere.

OUTPUT
------

The path will be created with the modified version of the text.
This file must not already exist.

If set to the special name `-`,
the XML data will be written to stdout.

If you really need to write the output to a file that already exists,
you can delete it beforehand,
or use `-` as the output file name
and pipe standard output into the file:

    linkify-references.py input.xml config.ini - > file-to-be-clobbered.xml

-h, --help
----------

If this flag is present on the command line,
the tool will print a brief summary of how to use itself,
then exit.

-v, --verbose
-------------

If present,
the tool will log informational messages as well as warnings and errors.
It can be repeated for greater (theoretical) verbosity,
but the tool might not have anything additional to say.

Configuration
=============

The configuration file is in the INI format
(specifically, the variant supported by Python's `ConfigParser` class).

It contains sections with names written in square brackets.
For example:

    [SomeSection]

Each section heading is followed by pairs of keys and values.
The key starts at the beginning of the line,
until an `=` sign,
then the rest of the line is the value.
For example:

    SomeKey = some value goes here

A value can be wrapped across multiple lines
if the following lines are indented:

    WordsToRemember =
        The LORD is my shepherd;
        I shall not want.
        He makes me lie down in green pastures;
        He leads me beside quiet waters.

If the first character on a line is `#`,
that line is a comment and is not part of a key or a value.

    # Not a key, even though it contains an = sign
    ActuallyAKey =
        # Not part of the value
        Part of the value

BookNames section
-----------------

Most of the keys in the `BookNames` section
are the Bible book abbreviations
from Appendix C of the OSIS 2.1.1 User's Guide,
or one of the [extended codes] from the CrossWire wiki.
The value for each key is
a comma separated list of names used for that book.
For example:

    Lam = Lamentations, Lament, Lam, Lm

The `NotABook` key in the `BookNames` section is special.
There are many ancient texts that didn't quite make it into the Biblical canon,
some of which are still occasionally cited,
using the same "chapter and verse" notation used for canonical texts.
For example, 2 Peter 2:4 refers to
events described in the non-canonical 1 Enoch 13:1-11 and 1 Enoch 20:1-4,
and some Bible texts will include a footnote to that effect.

Let's say you have the following patterns configured:

    [ReferencePatterns]
    AbsolutePatterns =
        BOOK CHAPTER:FROM_VERSE-TO_VERSE
    LocationRelativePatterns =
        CHAPTER:FROM_VERSE-TO_VERSE

If you leave 1 Enoch unmentioned,
the tool will skip over "1 Enoch",
see "13:1-11" as a location-relative reference,
and create a broken reference to "2 Peter 13:1-11".

To prevent this,
add "1 Enoch" to the `NotABook` key:

    [BookNames]
    NotABook = 1 Enoch

Then the tool will recognise the absolute reference,
ignore it because "1 Enoch" is Not A Book,
and keep going without creating any reference at all.

ReferencePatterns section
-------------------------

The keys in this section
describe how to recognise a reference.
Each pattern contains text (to be matched exactly)
and tokens (representing parts of the reference.
For example,
the pattern `BOOK CHAPTER:VERSE` contains:

  - the token `BOOK`,
    which matches one of the book names
    configured in the `BookNames` section above
  - a space, to be matched exactly
  - the token `CHAPTER`,
    which matches a number that will be interpreted as a chapter number
  - a colon, to be matched exactly
  - the token `VERSE`,
    which matches a number that will be interpreted as a verse number

Each key contains a list of patterns,
one per line,
and different keys represent different ways to interpret
the context of the reference.
For example, in a footnote like "Hebrew tannin; here and in verse 10"
the "10" part is a reference to verse 10 of the current chapter,
while in text like "See Daniel 4:7,10"
the "10" part is a reference to verse 10 in a different chapter.

Not every token is available for use with every key,
but the complete list of available tokens is:

  - `BOOK`
    matches one of the book names or abbreviations
    defined in the `BookNames` section
  - `CHAPTER`
    matches a number,
    interpreted as a chapter number
  - `FROM_CHAPTER`
    matches a number,
    interpreted as the beginning of a chapter range
    ending at the number matched by `TO_CHAPTER`
  - `FROM_NUMBER`
    matches a number which will be intepreted as a chapter or a verse
    depending granularity of the previous reference in the text,
    interpreted as the beginning of a range
    ending at the number matched by `TO_NUMBER`
  - `FROM_VERSE`
    matches a number,
    interpreted as the beginning of a verse range
    ending at the number matched by `TO_VERSE`
  - `NUMBER`
    matches a number which will be intepreted as a chapter or a verse
    depending granularity of the previous reference in the text
  - `TO_CHAPTER`
    matches a number,
    interpreted as the end of a chapter range
    that started at the number matched by `FROM_CHAPTER`
  - `TO_NUMBER`
    matches a number which will be intepreted as a chapter or a verse
    depending granularity of the previous reference in the text,
    interpreted as the end of a range
    that started at the number matched by `FROM_NUMBER`
  - `TO_VERSE`
    matches a number,
    interpreted as the end of a verse range
    that started at the number matched by `FROM_VERSE`
  - `VERSE`
    matches a number,
    interpreted as a verse number

The order of patterns in a key is important.
The tool goes down the patterns in the list,
looking for the first match it finds.
If you have a list of patterns like this:

    # These are in the wrong order!
    BOOK CHAPTER:VERSE
    BOOK CHAPTER:FROM_VERSE-TO_VERSE

then text like "1 John 4:7-8" will always match the first pattern,
leaving text "-8" which will not be recognised as a reference.
In general, the longest and most detailed patterns should go first,
and the shorter and more generic patterns should come after.

AbsolutePatterns key
--------------------

Absolute patterns must include a `BOOK` token,
may include one of the `CHAPTER` tokens,
and may then also include one of the `VERSE` tokens.
Since they include all the information required to pinpoint
a specific book, chapter, or verse,
they are always interpreted the same way.

Examples:

  - `BOOK FROM_CHAPTER:FROM_VERSE-TO_CHAPTER:TO_VERSE`
  - `BOOK CHAPTER:FROM_VERSE-TO_VERSE`
  - `BOOK CHAPTER:VERSE`
  - `BOOK FROM_CHAPTER-TO_CHAPTER`
  - `BOOK CHAPTER`

It's generally not a good idea to include a pattern that's only `BOOK`.
Not every mention of "Judges" is a reference to the book of Judges.

LocationRelativePatterns key
----------------------------

Location-relative patterns must *not* include a `BOOK` token,
and may include one of the `CHAPTER` tokens,
one of the `VERSE` tokens,
or both.
The missing parts of a location-relative reference
are taken from the location where the reference appears
(hence "location-relative").
The reference targets the book where the reference appears,
and unless the pattern includes one of the `CHAPTER` tokens,
the chapter where the reference appears.

Examples:

  - `chapters FROM_CHAPTER-TO_CHAPTER`
  - `chapters CHAPTER`
  - `chapter CHAPTER`
  - `verses FROM_VERSE-TO_VERSE`
  - `verses VERSE`
  - `verse VERSE`

There is a pattern in the list
that says "chapters" followed by a range of chapters,
but also a pattern that says "chapters" followed by a single chapter.
That pattern becomes useful with
the "reference chain" feature described below.

ReferenceRelativePatterns key
-----------------------------

Reference-relative patterns must *not* include a `BOOK` token,
and may include one of the `CHAPTER` tokens,
one of the `VERSE` tokens,
or both,
or one of the `NUMBER` tokens instead of either.
The missing parts of a reference-relative reference
are taken from the previously-matched reference in the current verse.

Examples:

  - `Idem CHAPTER:VERSE`
  - `Id CHAPTER:VERSE`

Reference-relative patterns do not often occur in modern Bibles.
Some older commentaries will use these patterns
to distinguish references to the passage they are discussing,
from references to another passage they are drawing parallels to.

Reference chains
----------------

A standalone reference needs to be verbose to be unambiguous,
like "Exodus 22:9".
However,
within a collection of references
like "Exodus 22;4, 9, and 10"
the reference to verse 9 is just a single digit
whose meaning is made clear by the context in which it appears.
"Reference chains" are the way this tool models such collections.

A chain consists of:

  - a non-chain reference
    (absolute, or location- or reference-relative)
  - text matching a pattern in `ReferenceChainLinks`
  - a reference matching a pattern in `ReferenceChainItems`
  - optionally, another link then another item, and so on

With a configuration like this:

    AbsolutePatterns =
        BOOK CHAPTER:VERSE
    ReferenceChainLinks =
        , and
        ,
    ReferenceChainItems =
        VERSE

...our reference chain example above breaks down into:

  - Absolute reference `Exodus 22:4`
  - Chain link `,`
  - Chain item `9` (Exodus 22:9)
  - Chain link `, and`
  - Chain item `10` (Exodus 22:10)

Reference chain items are reference-relative patterns,
but because they only match after a previous reference and a link,
they can be much more general.
If you put the simple pattern `VERSE` into `ReferenceRelativePatterns`,
every single number in the entire input file
would be treated as a reference,
which would almost certainly be wrong.

A simpler example of a reference chain
is something like "verses 5 and 7".
If `LocationRelativePatterns` includes `verses VERSE`,
we will recognise the first part of the chain,
but `and` is not (yet) one of the chain links.
That's easy enough to add:

    ReferenceChainLinks =
        , and
        ,
        and

... so now "verses 5 and 7" will be properly recognised.

However, chain items are not always verses:
"Maskil is probably a musical or liturgical term;
used for Psalms 32, 42, 44-45, 52-55, 74, 78, 88-89, and 142."
The `ReferenceChainLinks` we used above still work in this chain,
but the numbers are not verses, they're chapters.
We could insert the pattern `CHAPTER` above `VERSE` in our example config,
but then the numbers in our previous example would be detected as chapters.

Instead, for reference chains you should usually use
the special token `NUMBER`,
which is a number at the same granularity as the previous reference.
After "Exodus 22:4", a `NUMBER` is a verse;
after "Psalm 32", a `NUMBER` is a chapter.
With this configuration,
we can properly recognise all our example chains:

    AbsolutePatterns =
        BOOK CHAPTER:VERSE
        BOOK CHAPTER
    ReferenceChainLinks =
        , and
        ,
        and
    ReferenceChainItems =
        FROM_NUMBER-TO_NUMBER
        NUMBER

ReferenceChainLinks key
-----------------------

Reference chain links must not include any tokens,
just exact text.
The text is ignored,
but its presence confirms that the chain continues
the tool should check for a reference chain item next.

Examples:

  - `, and`
  - `,`
  - `and`

ReferenceChainItems key
-----------------------

Reference chain item patterns must *not* include a `BOOK` token,
and may include one of the `CHAPTER` tokens,
one of the `VERSE` tokens,
or both,
or one of the `NUMBER` tokens instead of either.
They are always interpreted relative to
the previous refererence in the chain.

Examples:

  - `CHAPTER:VERSE`
  - `FROM_NUMBER-TO_NUMBER`
  - `NUMBER`

As described under "Reference chains" above,
you generally want to use the `NUMBER` tokens in these patterns
rather than a lone `CHAPTER` or `VERSE` token,
so that you can handle both chains of chapters and chains of verses.

Errors and Warnings
===================

This tool can produce a variety of helpful informational and warning messages,
reporting issues with the configuration or the input file.

In the following sections,
"CONTEXT" is the OSIS ID that was most recently-seen
when the problem was detected.
For example, `John.3.16` refers to John 3:16.
Assuming your OSIS XML file uses IDs regularly,
searching it for the given OSIS ID
should get you close to the problem's location.

Cannot use TOKEN-A and TOKEN-B in the same pattern: PATTERN
-----------------------------------------------------------

Within a pattern, some tokens are mutually exclusive.
For example, it does not make sense
for a pattern to have both `CHAPTER` and `FROM_CHAPTER` tokens.

Consider:

  - Removing one or the other of these tokens
    from this pattern
    in your config file
  - Remove the pattern entirely

Got location-relative reference REF before first location?
----------------------------------------------------------

A location-relative reference
(one of the patterns defined in "LocationRelativePatterns" in the config file)
is interpreted relative to the location where it occurs.
If we encounter a location-relative reference
before the first `osisID` attribute in the input file,
we can't interpret the reference.

Consider:

  - Removing the pattern that matches this reference
    from LocationRelativePatterns
  - Editing the input file to add a missing `osisID` attribute
  - Editing the reference to make it absolute.

In CONTEXT, found suspicious fragment FRAG
------------------------------------------

In addition to searching the input file
for the patterns specified in the config file,
the tool searches non-reference text
for "suspicious fragments"
(such as a ":3" or "-7")
that are unlikely to appear in regular text
but often appear in references,
since they may indicate a reference not detected,
or incorrectly detected.

The usual cause of this message is a missing pattern.
If the "AbsolutePatterns" config option contains `BOOK CHAPTER`
but is missing `BOOK CHAPTER:VERSE`,
then tool will look at a reference like "John 3:16"
and see "John 3"
but skip over the ":16",
causing the message
"found suspicious fragment ':1'".

A less common cause of this message is
having patterns in the wrong order.
The tool matches the first pattern in its list,
so if an earlier pattern matches the same text
as the beginning of a later, longer pattern,
the later pattern will never be used.

For example,
if the config file contains:

```
AbsolutePatterns=
    BOOK CHAPTER
    BOOK FROM_CHAPTER-TO_CHAPTER
```

...and the text contains "Matthew 3-5",
the tool will recognise that "Matthew 3" matches the first pattern,
and never try the second pattern,
causing the message "suspicious fragment '-5'".

The solution here is to rearrange the list of patterns
to put longer patterns at the beginning.

Chapter must be natural number, not CHAPTER in osisID ID
--------------------------------------------------------

All the `osisID` attributes in the input file
are expected to contain one or more OSIS IDs
of the form "book", "book.chapter", or "book.chapter.verse",
such as `John.3.16`.

The CHAPTER part should be a natural number (1, 2, 3, etc.),
but it doesn't seem to be one.

Consider:

  - Editing the `osisID` attribute so the chapter is a natural number
  - Remove the `osisID` attribute entirely

In CONTEXT, reference-relative reference REF occurs before first reference?
---------------------------------------------------------------------------

A reference-relative reference
(matching one of the patterns defined in "ReferenceRelativePatterns"
or "ReferenceChainItems"
in the config file)
is interpreted relative to the most recently-seen reference
since we reached CONTEXT.
In a Bible text,
this means every verse must have an absolute or location-relative reference
before it can have a reference-relative reference.

Sometimes this may be accidentally triggered by text
that looks like it might be a reference but isn't.
For example,
if "ReferenceChainLinks" contains `and`,
"ReferenceChainItems" contains `CHAPTER`,
and your text contains a fragment like
"700 wives and 300 concubines",
the tool may see the "and 300" as a reference to chapter 300
of the same book as the previous reference.
If there is no such reference
(as is most likely),
you will get this warning.

Consider:

  - Removing the relevant patterns from ReferenceRelativePatterns
    or ReferenceChainItems
  - Editing the text so it doesn't match one of these patterns

In CONTEXT, reference REF has chapter numbers in the wrong order
----------------------------------------------------------------

In a reference that matches a pattern with `FROM_CHAPTER` and `TO_CHAPTER`,
the number that `FROM_CHAPTER` matched
was larger than
the number that `TO_CHAPTER` matched.

For example,
if "AbsolutePatterns" includes the pattern `BOOK FROM_CHAPTER-TO_CHAPTER`,
the text "Genesis 10-9" will trigger this message,
since chapter 10 comes after chapter 9, not before it.

Consider:

  - Swapping the order of the chapter numbers in the text
  - Editing or removing the relevant pattern so it no longer matches the text

In CONTEXT, reference REF has identical chapter numbers
-------------------------------------------------------

In a reference that matches a pattern with `FROM_CHAPTER` and `TO_CHAPTER`,
the number that `FROM_CHAPTER` matched
was the same as
the number that `TO_CHAPTER` matched.

For example,
if "AbsolutePatterns" includes the pattern `BOOK FROM_CHAPTER-TO_CHAPTER`,
the text "Genesis 9-9" will trigger this message,
since it mentions chapter 9 twice.

Consider:

  - Removing the redundant number from the text
  - Editing or removing the relevant pattern so it no longer matches the text

In CONTEXT, reference REF has identical verse numbers
-----------------------------------------------------

In a reference that matches a pattern with `FROM_VERSE` and `TO_VERSE`,
the number that `FROM_VERSE` matched
was the same as
the number that `TO_VERSE` matched.

For example,
if "AbsolutePatterns" includes the pattern `BOOK CHAPTER:FROM_VERSE-TO_VERSE`,
the text "Matthew 22:37-37" will trigger this message,
since it mentions verse 37 twice.

Consider:

  - Removing the redundant number from the text
  - Editing or removing the relevant pattern so it no longer matches the text

In CONTEXT, reference REF has verse numbers in the wrong order
--------------------------------------------------------------

In a reference that matches a pattern with `FROM_VERSE` and `TO_VERSE`,
the number that `FROM_VERSE` matched
was larger than
the number that `TO_VERSE` matched.

For example,
if "AbsolutePatterns" includes the pattern `BOOK CHAPTER:FROM_VERSE-TO_VERSE`,
the text "Matthew 22:40-37" will trigger this message,
since verse 40 comes after verse 37, not before it.

Consider:

  - Swapping the order of the verser numbers in the text
  - Editing or removing the relevant pattern so it no longer matches the text

In CONTEXT, NAME is not a book, ignoring reference REF
------------------------------------------------------

This message is purely informational,
and observes that the tool saw
a reference involving one of the names listed in the "NotABook" config key.

invalid literal for int ... Parsing chapter CHAPTER from osisID ID
------------------------------------------------------------------

All the `osisID` attributes in the input file
are expected to contain one or more OSIS IDs
of the form "book", "book.chapter", or "book.chapter.verse",
such as `John.3.16`.

The "chapter" component of the listed ID should be a natural number
(1, 2, 3, etc.)
but in this ID apparently it's not.

Consider:

  - Editing the input file to fix or remove the broken `osisID` attribute

invalid literal for int ... Parsing verse VERSE from osisID ID
--------------------------------------------------------------

All the `osisID` attributes in the input file
are expected to contain one or more OSIS IDs
of the form "book", "book.chapter", or "book.chapter.verse",
such as `John.3.16`.

The "verse" component of the listed ID should be a natural number
(1, 2, 3, etc.)
but in this ID apparently it's not.

Consider:

  - Editing the input file to fix or remove the broken `osisID` attribute

Missing TOKEN-A in pattern: PATTERN
-----------------------------------

Patterns in the "AbsolutePatterns" key in the config file
must include the `BOOK` token,
otherwise it the referenced book would have to be guessed from context,
making it a relative pattern instead of an absolute one.

Patterns that include `BOOK` and at least one of the `VERSE` tokens
must also include at least one of the `CHAPTER` tokens,
since it does not make sense to refere to a particular verse of a book
without also specifying which chapter the verse is in.
(like OSIS in general,
the tool does not treat single-chapter books specially).

Consider:

  - Moving a pattern without `BOOK`
    to "LocationRelativePatterns" or "ReferenceRelativePatterns"
  - Adding the requested token to the pattern
  - Editing the text to ensure references to single-chapter books
    always include the chapter number 1

Missing TOKEN-A to go with TOKEN-B in pattern: PATTERN
------------------------------------------------------

Within a pattern, some tokens must be used in pairs.
For example,
a pattern that uses `FROM_CHAPTER`
must also contain `TO_CHAPTER`
to represent a complete span of chapters.
If a pattern only needs one chapter,
it should use the `CHAPTER` token
instead of `FROM_CHAPTER` or `TO_CHAPTER` individually.

Consider:

  - Adding the suggested missing token to the pattern
  - Removing the other token from the pattern
  - Replacing the other token with one that doesn't need a pair

osisID must be book.chapter.verse: ID
-------------------------------------

All the `osisID` attributes in the input file
are expected to contain one or more OSIS IDs
of the form "book", "book.chapter", or "book.chapter.verse",
such as `John.3.16`.

This alleged `osisID` seems to contain more parts than expected,
so the tool doesn't know how to handle it.

Consider:

  - Editing the input file to fix or remove the broken `osisID` attribute

Pattern contains no tokens: PATTERN
-----------------------------------

Each pattern can contain fixed text
as well as tokens that represent part of a reference.
For example,
the pattern `verses FROM_VERSE-TO_VERSE` contains:

  - fixed text `verses `
  - token `FROM_VERSE`
  - fixed text `-`
  - token `TO_VERSE`

If a pattern contains only fixed text and not tokens,
the tool cannot recognise which parts represent the reference.

Consider:

  - Making sure you're only using
    tokens listed in the "Configuration" section of this documentation
  - Making sure the tokens in the pattern are all-uppercase,
    using an underscore to join separate parts
    (like `FROM_VERSE`)
  - Deleting the broken pattern

Token TOKEN should not be in pattern PATTERN
--------------------------------------------

Location- and reference-relative patterns
refer to a different part of the current book
or the prefiously-referenced book (respectively).
Therefore,
they cannot contain the `BOOK` token,
since that would make them absolute references.

Consider:

  - Moving the pattern to the "AbsolutePatterns" key of the config file.
  - Deleting the unwanted token from the pattern
  - Deleting the pattern entirely

Token TOKEN used twice in pattern: PATTERN
------------------------------------------

The tokens in the pattern tell the tool
how to interpret the parts of the reference.
If a pattern included two `BOOK` tokens, for example,
the tool wouldn't know which book the reference should point to.

Consider:

  - Removing one of the copies of the token from the pattern

Unrecognised OSIS book ID BOOK: ID
----------------------------------

All the `osisID` attributes in the input file
are expected to contain one or more OSIS IDs
of the form "book", "book.chapter", or "book.chapter.verse",
such as `John.3.16`.

If the book name is not one of the standard abbreviations
from Appendix C of the OSIS 2.1.1 User's Manual,
or one of the [extended codes] from the CrossWire wiki,
the tool does not know what text the input file is relevant to.

[extended codes]: http://wiki.crosswire.org/OSIS_Book_Abbreviations

Consider:

  - Editing the `osisID` attribute in the input file
    to use a recognised book abbreviation
  - Deleting the `osisID` attribute from the input file

Verse must be natural number, not VERSE in osisID ID
----------------------------------------------------

All the `osisID` attributes in the input file
are expected to contain one or more OSIS IDs
of the form "book", "book.chapter", or "book.chapter.verse",
such as `John.3.16`.

The VERSE part should be a natural number (1, 2, 3, etc.),
but it doesn't seem to be one.

Consider:

  - Editing the `osisID` attribute so the verse is a natural number
  - Remove the `osisID` attribute entirely

Hacking
=======

If you wish to modify the script,
you can run the built-in test suite like this:

    python3 -m unittest ./linkify-references.py

To Do
=====

Possible future improvements:

  - Warn if the config file has no book names
  - Warn if the config file has no patterns
  - Warn if the config file maps one book name to multiple abbreviations
  - Warn if the `NUMBER` token occurs in
    an absolute or location-relative reference
  - Count how many times each reference pattern is used,
    warn about unused patterns.
  - A way to quiet individual "suspicious fragment" warnings.
  - Warn about earlier patterns that are a prefix of later patterns.
  - Recognise chapter numbers in Roman numerals.
  - Recognise references without a chapter number to single-chapter books
    (like Jude)
  - Only scan non-canonical text for references,
    instead of scanning everything.
      - This will probably make things faster and more reliable,
        but will require many fixes to the unittests.
